function randBetween(min,max) {

	var number = Math.floor(Math.random()*(max-min+1)+min);
	//console.log(number);
    return number;
}


$(function() {
	
$(".fish").each(function() {
	
	var id = $(this).attr("id").split("_")[1];
	
	$.keyframe.define([{
	    name: 'swim' + id,
		'from' : {'left' : (randBetween(20,80)) + '%' , 'width' : '42px'},
	    '20%': {'left': '0','transform' : 'scaleX(1)','z-index' : '90'},
		'20.5%': {'transform' : 'scaleX(-1)','z-index' : '10','left': '-30px'},
		'21%': {'left': '80px'},
	    '42%': {'left': '60%'},
	    '50%': {'z-index' : '28','top' : (randBetween(30,70)) + '%','left' : (randBetween(60,75)) + '%','transform' : 'scaleX(-1)'},
		'50.5%': {'z-index' : '100','top' : (randBetween(30,70)) + '%','left' : (randBetween(60,75)) + '%','transform' : 'scaleX(1)'},
		'51%': {'left': (randBetween(10,70)) + '%','z-index' : '150'},
		'60%': {'left': (randBetween(10,70)) + '%','z-index' : '150'},
		'64%': {'left': (randBetween(10,70)) + '%','z-index' : '150'},
		'66%': {'left': (randBetween(10,70)) + 'px','z-index' : '150'},
		'69%': {'left': '-30px','z-index' : '150'},
		'70%': {'z-index' : '90','top' : (randBetween(15,40)) + '%','left' : '-30px','transform' : 'scaleX(1)'},
		'70.5%': {'z-index' : '10','left' : '-30px','transform' : 'scaleX(-1)'},
		'71%': {'left': (randBetween(10,70)) + 'px'},
		'80%': {'left': (randBetween(10,70)) + '%'},
		'81%': {'left': (randBetween(10,70)) + '%','top': (randBetween(40,70)) + '%'},
		'82%': {'left': (randBetween(10,80)) + '%','transform' : 'scaleX(-1)'},
		'82.5%': {'left': (randBetween(60,80)) + '%','top': '45%','transform' : 'scaleX(1)'},
	    '84%': {'z-index' : '28','top' : (randBetween(50,70)) + '%','left' : (randBetween(60,80)) + '%'},
		'88%': {'z-index' : '28','top' : (randBetween(50,70)) + '%','left' : (randBetween(60,80)) + '%'},
		'to': {'left': (randBetween(20,80)) + '%'}
	}]);
	
	var images = ["blue-fish.png","gold-fish.png","gold-fish2.png","green-fish.png","tropical-fish.png","whale.png"];
	
	var randomImage = images[randBetween(0,images.length - 1)];
	
	$("#img-fish_" + id).attr('src','./img/aquarium/' + randomImage);
	
	$(this).css("top",randBetween(10,70) + "%");
	
	$(this).playKeyframe(
		    "swim" + id + " 30s linear 0s infinite normal forwards",
		    function() {}
	);
});

});