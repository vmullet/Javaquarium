<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="right">
	<a href="<%=Forward.GOTO_CHANGE_LOCALE%>?<%=Param.LOCALE_NAME%>=en"><img
		src="./img/blank.gif" class="flag flag-gb right locale"
		alt="<bean:message
								key="locale.english" />"
		data-toggle="tooltip" data-placement="bottom"
		title="<bean:message
								key="locale.english" />" /></a> <a
		href="<%=Forward.GOTO_CHANGE_LOCALE%>?<%=Param.LOCALE_NAME%>=fr"><img
		src="./img/blank.gif" class="flag flag-fr right locale"
		alt="<bean:message
								key="locale.french" />"
		data-toggle="tooltip" data-placement="bottom"
		title="<bean:message
								key="locale.french" />" /></a>
</div>