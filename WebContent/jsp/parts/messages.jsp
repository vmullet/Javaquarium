<%@page import="com.javaquarium.consts.MessageType"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<!-- IF message of type ERRORS are present -->
				<logic:messagesPresent message="false">
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<html:errors />
					</div>
				</logic:messagesPresent>

				<logic:messagesPresent message="true" property="<%=MessageType.ERROR.toString()%>">
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<html:messages id="message" property="<%=MessageType.ERROR.toString()%>" message="true" />
						<bean:write name="message" filter="false" />
					</div>
				</logic:messagesPresent>

				<!-- IF message of type INFO are present -->
				<logic:messagesPresent message="true" property="<%=MessageType.INFO.toString()%>">
					<div class="alert alert-info">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<html:messages id="message" property="<%=MessageType.INFO.toString()%>" message="true" />
						<bean:write name="message" filter="false" />
					</div>
				</logic:messagesPresent>
				
				<!-- IF message of type WARNING are present -->
				<logic:messagesPresent message="true" property="<%=MessageType.WARNING.toString()%>">
					<div class="alert alert-warning">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<html:messages id="message" property="<%=MessageType.WARNING.toString()%>" message="true" />
						<bean:write name="message" filter="false" />
					</div>
				</logic:messagesPresent>
				
				<!-- IF message of type SUCCESS are present -->
				<logic:messagesPresent message="true" property="<%=MessageType.SUCCESS.toString()%>">
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<html:messages id="message" property="<%=MessageType.SUCCESS.toString()%>" message="true" />
						<bean:write name="message" filter="false" />
					</div>
				</logic:messagesPresent>