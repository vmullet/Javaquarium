<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.consts.MessageType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <%@ include file="/jsp/parts/header.jsp" %>
      <title>
         <bean:message key="page.title.ajouter_poisson" />
      </title>
   </head>
   <body>
      <div class="container">
         <%@ include file="/jsp/parts/locale.jsp" %>
         <h1>
            <bean:message key="ajoutPoisson.form.title" />
         </h1>
         <img src="./img/hank-dory-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <%@ include file="/jsp/parts/messages.jsp" %>
               <html:form action="/addPoisson" styleClass="form-horizontal">
                  <div class="form-group">
                     <label for="espece" class="control-label col-sm-4">
                        <bean:message
                           key="ajoutPoisson.form.name" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="addPoissonForm" property="espece" styleClass="form-control" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="description" class="control-label col-sm-4">
                        <bean:message
                           key="ajoutPoisson.form.description" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:textarea name="addPoissonForm" property="description" styleClass="form-control" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="couleur" class="control-label col-sm-4">
                        <bean:message
                           key="ajoutPoisson.form.couleur" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="addPoissonForm" property="couleur" styleClass="form-control" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="prix" class="control-label col-sm-4">
                        <bean:message
                           key="ajoutPoisson.form.prix" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="addPoissonForm" property="prix" styleClass="form-control" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="dimension" class="control-label col-sm-4">
                        <bean:message
                           key="ajoutPoisson.form.dimensions" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="addPoissonForm" property="dimension" styleClass="form-control" />
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                           <a class="btn btn-info btn-md" href="<%=Forward.GOTO_LISTE_ESPECE%>">
                              <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                              <bean:message key="message.return_my_aquarium" />
                           </a>
                           <html:submit styleClass="btn btn-success col-sm-3">
                              <bean:message key="ajoutPoisson.form.validate" />
                           </html:submit>
                           <html:reset styleClass="btn btn-warning col-sm-3">
                              <bean:message key="ajoutPoisson.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
               </html:form>
            </div>
         </div>
      </div>
      <%@ include file="/jsp/parts/footer.jsp" %>
   </body>
</html>