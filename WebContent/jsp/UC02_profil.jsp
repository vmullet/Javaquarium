<%@page import="com.javaquarium.consts.SessionVar"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<bean:define id="userCode" name="<%=SessionVar.USER.toString()%>" property="id"/>
<bean:define id="userLogin" name="<%=SessionVar.USER.toString()%>" property="username"/>
<bean:define id="userName" name="<%=SessionVar.USER.toString()%>" property="name"/>

   <head>
      <!-- CSS files -->
      <%@ include file="/jsp/parts/header.jsp" %>
      <title>
         <bean:message key="page.title.user_profile" />
      </title>
   </head>
   <body>
      <div class="container">
         <!-- Locale switcher with flags -->
         <%@ include file="/jsp/parts/locale.jsp" %>
         <h1>
            <bean:message key="editProfile.form.title" />
            <%=userLogin.toString()%>
         </h1>
         <img src="./img/turtle-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <!-- Message boxes for warnings/errors/info -->
               <%@ include file="/jsp/parts/messages.jsp" %>
               <!-- Beginning Form -->
               <html:form action="/editProfile" focus="name" styleClass="form-horizontal">
               	<html:hidden name="profileForm" property="userId" value="<%=userCode.toString() %>"/>
               	<html:hidden name="profileForm" property="username" value="<%=userLogin.toString() %>"/>
               	<!-- User name -->
                  <div class="form-group">
                     <label for="username" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.username" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <input type="text" class="form-control" id="usr" value="<%=userLogin.toString() %>" disabled>
                     </div>
                  </div>
                  <!-- Name -->
                  <div class="form-group">
                     <label for="name" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.name" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="profileForm" property="name" styleClass="form-control" value="<%=userName.toString() %>" />
                     </div>
                  </div>
                  <!-- Change Password ? -->
                  <div class="form-group">
                     <label for="changePassword" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.change_password" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:checkbox name="profileForm" property="changePassword" styleClass="checkbox" />
                     </div>
                  </div>
                  
                  <!-- Password -->
                  <div class="form-group">
                     <label for="password" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.new_password" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="profileForm" property="newPassword" styleClass="form-control switch"  />
                     </div>
                  </div>
                  <!-- Password again -->
                  <div class="form-group">
                     <label for="password2" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.new_password2" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="profileForm" property="newPassword2" styleClass="form-control switch" />
                     </div>
                  </div>
                  <!-- Buttons -->
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                        	<a class="btn btn-info btn-md" href="<%=Forward.GOTO_LISTE_ESPECE%>">
                  				<bean:message key="message.return" />
               				</a>
                           <html:submit styleClass="btn btn-success col-sm-3">
                              <bean:message key="editProfile.form.validate" />
                           </html:submit>
                           <html:reset styleClass="btn btn-warning col-sm-3">
                              <bean:message key="editProfile.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
                  <!-- End form -->
               </html:form>
            </div>
         </div>
      </div>
      <!-- JS files -->
      <%@ include file="/jsp/parts/footer.jsp" %>
   </body>
</html>