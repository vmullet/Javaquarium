package com.javaquarium.consts;

/**
 * A final class to store the diferent parameters names used in jsp files
 * (useful to synchronise code between jsp and actions)
 * 
 * @author Valentin
 *
 */
public final class Param {

	public static final String ESPECE_ID = "especeId";

	public static final String USER_ID = "userId";

	public static final String LOCALE_NAME = "localeName";

	/**
	 * DON'T CALL ME !!!
	 */
	private Param() {
	}

}
