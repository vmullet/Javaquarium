package com.javaquarium.consts;

/**
 * Final class to store the different types of Forward It is used for mapping
 * return or in jsp for <a href> links
 * 
 * @author Valentin
 *
 */
public final class Forward {

	/**
	 * Constant name of the variable to forward in case of success (used for mapping
	 * return in actions)
	 */
	public static final String SUCCESS = "success";

	/**
	 * Constant name of the variable to forward in case of failure (used for mapping
	 * return in actions)
	 */
	public static final String FAIL = "fail";

	/**
	 * Constant name of the forward if the user is not logged (used for mapping
	 * return in actions)
	 */
	public static final String NOT_LOGGED = "not_logged";

	/**
	 * Constant name of the action page to go to the login page (used in jsp <a
	 * href>)
	 */
	public static final String GOTO_LOGIN = "gotoLogin.do";

	/**
	 * Constant name of the action page to register a new user (used in jsp <a
	 * href>)
	 */
	public static final String GOTO_REGISTER = "gotoRegister.do";

	/**
	 * Constant name of the action page to access to my profile (used in jsp <a
	 * href>)
	 */
	public static final String GOTO_MY_PROFILE = "gotoMyProfile.do";

	/**
	 * Constant name of the action to change the current locale (used in jsp <a
	 * href>)
	 */
	public static final String GOTO_CHANGE_LOCALE = "changeLocale.do";

	/**
	 * Constant name of the action page to list the species (used in jsp <a href>)
	 */
	public static final String GOTO_LISTE_ESPECE = "listerEspece.do";

	/**
	 * Constant name of the action page to list the species (used in jsp <a href>)
	 */
	public static final String GOTO_LIVE_AQUARIUM = "liveAquarium.do";

	/**
	 * Constant name of the action page to add a new Poisson (used in jsp <a href>)
	 */
	public static final String GOTO_AJOUT = "gotoAjout.do";

	/**
	 * Constant name of the action page to logout the user (used in jsp <a href>)
	 */
	public static final String GOTO_LOGOUT = "logout.do";

	/**
	 * Constant name of the action page to populate the edit form for the poisson
	 */
	public static final String GOTO_EDIT_POISSON = "popeditPoisson.do";

	/**
	 * Constant name of the action page to remove a poisson (used in jsp <a href>)
	 */
	public static final String GOTO_REMOVE_POISSON = "removePoisson.do";

	/**
	 * Constant name of the action page to add a poisson to the aquarium (used in
	 * jsp <a href>)
	 */
	public static final String GOTO_AJOUT_AQUARIUM = "addAquarium.do";

	/**
	 * Constant name of the action page to remove a poisson from the aquarium (used
	 * in jsp <a href>)
	 */
	public static final String GOTO_ENLEVE_AQUARIUM = "removeAquarium.do";

	/**
	 * Constant name of the action page to save the aquarium (used in jsp <a href>)
	 */
	public static final String GOTO_SAVE_AQUARIUM = "saveAquarium.do";

	/**
	 * Constant name of the action page to clear the aquarium (used in jsp <a href>)
	 */
	public static final String GOTO_CLEAR_AQUARIUM = "clearAquarium.do";

	/**
	 * DON'T CALL ME !!!
	 */
	private Forward() {
	}

}
