package com.javaquarium.consts;

/**
 * A final class which contains all error codes Error codes starts from -100 to
 * -20000 for errors concerning users Errors above -20000 are for developers
 * 
 * @author Valentin
 *
 */
public final class ExceptionCode {

	/**
	 * The code value limit for errors destinated to developers or users (check
	 * ServiceException class)
	 */
	public static final int LIMIT_ERROR_FOR_USERS = -20000;

	/**
	 * The error code value for database connection issue
	 */
	public static final int ERROR_DATABASE_CONNECTION = -20001;

	/**
	 * The error code value when trying to insert a duplicated user
	 */
	public static final int ERROR_DUPLICATE_USER = -100;

	/**
	 * The error code value when trying to get an element which doesn't exist
	 */
	public static final int ERROR_ELEMENT_NOT_EXIST = -110;

	/**
	 * The error code when trying to insert a duplicated poisson
	 */
	public static final int ERROR_DUPLICATE_POISSON = -120;

	/**
	 * The error code value when trying to get unique result but there are more than
	 * 1
	 */
	public static final int ERROR_GET_NONUNIQUE = -130;

	/**
	 * DON'T CALL ME !!!
	 */
	private ExceptionCode() {
	}

}
