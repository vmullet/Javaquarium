package com.javaquarium.dao;

import java.util.List;

import com.javaquarium.beans.data.PoissonDO;
import com.javaquarium.exception.DAOException;

/**
 * Classic DAO Interface
 * 
 * @author Valentin
 *
 */
public interface IPoissonDAO {

	/**
	 * Classic method to get all the PoissonDO from the database
	 * 
	 * @return a List of all the PoissonDO
	 */
	List<PoissonDO> getAll() throws DAOException;

	/**
	 * Classic method to retrieve a single PoissonDO based on his name
	 * 
	 * @param name
	 *            The name of the PoissonDO to retrieve
	 * @return The PoissonDO retrieved (or null if not)
	 */
	PoissonDO getOneByName(final String name) throws DAOException;

	/**
	 * Classic method to retrieve a single PoissonDO based on his id
	 * 
	 * @param especeId
	 *            The id of the PoissonDO to retrieve
	 * @return The PoissonDO retrieved (or null if not)
	 */
	PoissonDO getOneById(final int especeId) throws DAOException;

	/**
	 * Classic method to add a new PoissonDO to the database
	 * 
	 * @param pdo
	 *            The PoissonDO to add
	 */
	void addOne(final PoissonDO pdo) throws DAOException;

	/**
	 * Classic method to update a PoissonVO
	 * 
	 * @param pdo
	 *            The poissonVO to update
	 * @throws DAOException
	 *             in case of error
	 */
	void update(final PoissonDO pdo) throws DAOException;

	/**
	 * Classic method to remove a PoissonDO
	 * 
	 * @param pdo
	 *            The poissonVO to remove
	 * @throws DAOException
	 *             in case of error
	 */
	void removeOne(final PoissonDO pdo) throws DAOException;

	/**
	 * Classic method to remove a PoissonDO with his id
	 * 
	 * @param pdo
	 *            The poissonVO to remove
	 * @throws DAOException
	 *             in case of error
	 */
	void removeOne(final int especeId) throws DAOException;
}
