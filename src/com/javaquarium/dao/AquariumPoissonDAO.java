package com.javaquarium.dao;

import java.util.List;

import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.JDBCConnectionException;

import com.javaquarium.beans.data.AquariumPoissonDO;
import com.javaquarium.consts.ExceptionCode;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.exception.DAOException;
import com.javaquarium.util.HibernateUtils;

/**
 * Classic DAO class to manage AquariumPoissonDO and interact with database
 * 
 * @author Valentin
 *
 */
public class AquariumPoissonDAO implements IAquariumPoissonDAO {

	@Override
	public void addNewAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.save(aPdo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}

	}

	@Override
	public void updateAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.update(aPdo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}

	}

	@Override
	public void removeAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.delete(aPdo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}

	}

	@Override
	public void removeAllAquariumPoisson(final int userId) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		final Query query = session.createQuery("delete AquariumPoissonDO where user.id = :user_id");
		query.setParameter("user_id", userId);
		try {
			query.executeUpdate();
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AquariumPoissonDO> getAllAquariumPoisson(final int userId) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session.createQuery("from AquariumPoissonDO where user.id = :user_id");
		query.setParameter("user_id", userId);
		try {
			final List<AquariumPoissonDO> result = query.list();
			return result;
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}

	}

	@Override
	public AquariumPoissonDO getAquariumPoisson(final int userId, final int especeId) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session
				.createQuery("from AquariumPoissonDO ado where ado.user.id = :user_id and ado.espece.id = :espece");
		query.setParameter("user_id", userId);
		query.setParameter("espece", especeId);
		try {
			final AquariumPoissonDO result = (AquariumPoissonDO) query.uniqueResult();
			return result;
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final NonUniqueResultException nre) {
			throw new DAOException(ExceptionCode.ERROR_GET_NONUNIQUE, MessageKey.ERROR_GET_NONUNIQUE,
					nre.getMessage().toString());
		} finally {
			session.close();
		}

	}

	@Override
	public int getTotalAquariumPoissons(final int userId) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session
				.createQuery("select sum(ado.quantity) from AquariumPoissonDO ado where ado.user.id = :user_id");
		query.setParameter("user_id", userId);
		try {
			final Object result = query.uniqueResult();
			if (result == null)
				return 0;
			else
				return Integer.parseInt(result.toString());
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_GET_NONUNIQUE);
		} finally {
			session.close();
		}

	}

}
