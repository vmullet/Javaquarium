package com.javaquarium.dao;

import java.util.List;

import com.javaquarium.beans.data.AquariumPoissonDO;
import com.javaquarium.exception.DAOException;

public interface IAquariumPoissonDAO {

	void addNewAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException;

	void updateAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException;

	void removeAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException;

	void removeAllAquariumPoisson(final int userId) throws DAOException;

	List<AquariumPoissonDO> getAllAquariumPoisson(final int userId) throws DAOException;

	AquariumPoissonDO getAquariumPoisson(final int userLogin, final int especeId) throws DAOException;

	int getTotalAquariumPoissons(final int userId) throws DAOException;

}
