package com.javaquarium.dao;

import com.javaquarium.beans.data.UserDO;
import com.javaquarium.exception.DAOException;

/**
 * Classic DAO Interface
 * 
 * @author Valentin
 *
 */
public interface IUserDAO {

	/**
	 * Method to get a UserDO (user) based on a username and a password
	 * 
	 * @param username
	 *            The username searched
	 * @param password
	 *            The password searched
	 * @return The LoginDO (if it exist) based on this combination
	 */
	UserDO getOne(final String username, final String password) throws DAOException;

	/**
	 * Method to get a UserDO based on his id
	 * 
	 * @param userId
	 *            The id of the user searched
	 * @return The userDO found if it exists
	 * @throws DAOException
	 *             if there's a problem
	 */
	UserDO getOneById(final int userId) throws DAOException;

	/**
	 * Method to add a new user
	 * 
	 * @param uDo
	 *            The user to add
	 * @throws DAOException
	 *             if there's a problem
	 */
	void addOne(final UserDO uDo) throws DAOException;

	/**
	 * Method to update user's information
	 * 
	 * @param uDo
	 *            The user to update
	 * @throws DAOException
	 *             if there's a problem
	 */
	void update(final UserDO uDo) throws DAOException;
}
