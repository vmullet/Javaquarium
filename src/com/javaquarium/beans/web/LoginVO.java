package com.javaquarium.beans.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.javaquarium.consts.MessageType;

/**
 * JavaBean class to represent a LoginForm
 * 
 * @author Valentin
 *
 */
public class LoginVO extends ActionForm {

	private static final long serialVersionUID = -6367748115823590248L;
	private String username;
	private String password;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	@Override
	public void reset(final ActionMapping mapping, final HttpServletRequest request) {
		this.username = "";
		this.password = "";
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest req) {
		final ActionErrors errors = new ActionErrors();

		if (this.username.length() == 0) {
			errors.add(MessageType.ERROR.toString(), new ActionMessage("error.login.no_username"));
		}

		if (this.password.length() == 0) {
			errors.add(MessageType.ERROR.toString(), new ActionMessage("error.login.no_password"));
		} else {
			if (this.password.length() < 4) {
				errors.add(MessageType.ERROR.toString(), new ActionMessage("error.login.password_too_short"));
			}
		}

		return errors;

	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Type : " + getClass().getName().toString() + "\n");
		sb.append("Nom d'utilisateur : " + this.username + "\n");
		sb.append("Mot de passe : " + this.password + "\n");

		return sb.toString();
	}

}
