package com.javaquarium.beans.data;

/**
 * JavaBean class to represent an AquariumPoissonVO
 * 
 * @author Valentin
 *
 */
public class AquariumPoissonVO {

	private int id;

	private PoissonVO espece;

	private int quantitySaved;

	private int quantityNotSaved;

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * @return the espece
	 */
	public PoissonVO getEspece() {
		return this.espece;
	}

	/**
	 * @param espece
	 *            the espece to set
	 */
	public void setEspece(final PoissonVO espece) {
		this.espece = espece;
	}

	/**
	 * @return the quantitySaved
	 */
	public int getQuantitySaved() {
		return this.quantitySaved;
	}

	/**
	 * @param quantitySaved
	 *            the quantitySaved to set
	 */
	public void setQuantitySaved(final int quantitySaved) {
		this.quantitySaved = quantitySaved;
	}

	/**
	 * @return the quantityNotSaved
	 */
	public int getQuantityNotSaved() {
		return this.quantityNotSaved;
	}

	/**
	 * @param quantityNotSaved
	 *            the quantityNotSaved to set
	 */
	public void setQuantityNotSaved(final int quantityNotSaved) {
		this.quantityNotSaved = quantityNotSaved;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Type : " + getClass().getName() + "\n");
		sb.append("Id : " + this.id + "\n");
		sb.append("Espece : " + this.espece.toString() + "\n");
		sb.append("Quantit� Sauvegard�e : " + this.quantitySaved + "\n");
		sb.append("Quantit� Non Sauvegard�e : " + this.quantityNotSaved + "\n");
		return sb.toString();
	}

}
