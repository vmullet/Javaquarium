package com.javaquarium.exception;

import org.apache.struts.action.ActionMessage;

/**
 * Interface to define a classic ServiceException
 * 
 * @author Valentin
 *
 */
public interface IServiceException {

	/**
	 * Classic getter
	 * 
	 * @return The error code
	 */
	int getCode();

	/**
	 * Classic getter
	 * 
	 * @return The messagekey
	 */
	String getMessageKey();

	/**
	 * Classic getter
	 * 
	 * @return The cause object of this exception
	 */
	Object getCauseObject();

	/**
	 * Method to print the error clearly in console for developers
	 */
	void printError();

	/**
	 * Method to know if the error description must be shown to users too (if
	 * not,they see default error message)
	 * 
	 * @return True or False based on errorCode value
	 */
	boolean isForUsers();

	/**
	 * Method to get the ActionMessage based on this exception
	 * 
	 * @param args
	 *            Parameters for ApplicationResource message (check {0,1...} in
	 *            ApplicationResource.properties files)
	 * @return The ActionMessage shown to users
	 */
	ActionMessage getError(String[] args);

}
