package com.javaquarium.business;

import com.javaquarium.beans.data.UserDO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.beans.web.ProfileVO;
import com.javaquarium.beans.web.RegisterVO;
import com.javaquarium.exception.DAOException;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueDAO;

/**
 * Classic service to manage users
 * 
 * @author Valentin
 *
 */
public class UserService implements IUserService {

	/**
	 * The catalogueDAO to communicate with DAOs (instantiated by Spring IoC)
	 */
	private CatalogueDAO catalogueDAO;

	/**
	 * @param catalogueDAO
	 *            the catalogueDAO to set
	 */
	public void setCatalogueDAO(final CatalogueDAO catalogueDAO) {
		this.catalogueDAO = catalogueDAO;
	}

	@Override
	public UserVO getOne(final String username, final String password) throws ServiceException {

		try {
			final UserDO lDo = this.catalogueDAO.getUserDAO().getOne(username, password);
			if (lDo == null)
				return null;
			return getVOfromDO(lDo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public UserVO getOneById(final int userId) throws ServiceException {
		try {
			final UserDO lDo = this.catalogueDAO.getUserDAO().getOneById(userId);
			if (lDo == null)
				return null;
			return getVOfromDO(lDo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public void addOne(final RegisterVO user) throws ServiceException {
		final UserDO uDo = getDOfromForm(user);
		try {
			this.catalogueDAO.getUserDAO().addOne(uDo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}
	}

	@Override
	public void update(final ProfileVO user) throws ServiceException {
		try {
			final UserDO updatedUser = getDOfromForm(user);
			// if the user don't change the password
			if (user.getNewPassword().isEmpty()) {
				final UserDO currentUser = this.catalogueDAO.getUserDAO().getOneById(user.getUserId());
				updatedUser.setPassword(currentUser.getPassword());
			}
			this.catalogueDAO.getUserDAO().update(updatedUser);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public UserVO getVOfromDO(final UserDO lDo) {
		final UserVO lVo = new UserVO();

		lVo.setId(lDo.getId());
		lVo.setUsername(lDo.getUsername());
		lVo.setName(lDo.getName());
		return lVo;
	}

	@Override
	public UserDO getDOfromVO(final UserVO lVo) {
		final UserDO uDo = new UserDO();

		uDo.setId(lVo.getId());
		uDo.setUsername(lVo.getUsername());
		uDo.setName(lVo.getName());
		return uDo;
	}

	@Override
	public UserDO getDOfromForm(final RegisterVO userData) {
		final UserDO uDo = new UserDO();

		uDo.setName(userData.getName());
		uDo.setUsername(userData.getUsername());
		uDo.setPassword(userData.getPassword());
		return uDo;

	}

	@Override
	public UserDO getDOfromForm(final ProfileVO userData) {
		final UserDO uDo = new UserDO();

		uDo.setId(userData.getUserId());
		uDo.setName(userData.getName());
		uDo.setUsername(userData.getUsername());
		uDo.setPassword(userData.getNewPassword());

		return uDo;

	}

}
