package com.javaquarium.business;

import java.util.ArrayList;
import java.util.List;

import com.javaquarium.beans.data.PoissonDO;
import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.consts.ExceptionCode;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.exception.DAOException;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueDAO;

/**
 * Classic Service
 * 
 * @author Valentin
 *
 */
public class PoissonService implements IPoissonService {

	/**
	 * The catalogueDAO to communicate with DAOs (instantiated by Spring IoC)
	 */
	private CatalogueDAO catalogueDAO;

	/**
	 * @param catalogueDAO
	 *            the catalogueDAO to set
	 */
	public void setCatalogueDAO(final CatalogueDAO catalogueDAO) {
		this.catalogueDAO = catalogueDAO;
	}

	@Override
	public List<PoissonVO> getAll() throws ServiceException {

		final List<PoissonVO> listePvo = new ArrayList<PoissonVO>();

		try {
			final List<PoissonDO> listePdo = this.catalogueDAO.getPoissonDAO().getAll();
			for (final PoissonDO pDo : listePdo) {
				final PoissonVO pVo = getVOfromDO(pDo);
				listePvo.add(pVo);
			}
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

		return listePvo;

	}

	@Override
	public PoissonVO getOneById(final int especeId) throws ServiceException {
		PoissonDO pDo;
		try {
			pDo = this.catalogueDAO.getPoissonDAO().getOneById(especeId);
			if (pDo == null)
				return null;
			return getVOfromDO(pDo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}
	}

	@Override
	public PoissonVO getOneByName(final String name) throws ServiceException {
		PoissonDO pDo;
		try {
			pDo = this.catalogueDAO.getPoissonDAO().getOneByName(name);
			if (pDo == null)
				return null;
			return getVOfromDO(pDo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public void addOne(final PoissonVO pvo) throws ServiceException {
		final PoissonDO toInsert = getDOfromVO(pvo);
		try {
			this.catalogueDAO.getPoissonDAO().addOne(toInsert);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(ExceptionCode.ERROR_DUPLICATE_POISSON, MessageKey.ERROR_DUPLICATE_POISSON,
					toInsert);
		}

	}

	@Override
	public void update(final PoissonVO pvo) throws ServiceException {
		final PoissonDO toUpdate = getDOfromVO(pvo);
		try {
			this.catalogueDAO.getPoissonDAO().update(toUpdate);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION,
					toUpdate);
		}

	}

	@Override
	public void removeOne(final PoissonVO pvo) throws ServiceException {
		final PoissonDO toRemove = getDOfromVO(pvo);
		try {
			this.catalogueDAO.getPoissonDAO().removeOne(toRemove);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION,
					toRemove);
		}

	}

	@Override
	public void removeOne(final int especeId) throws ServiceException {
		try {
			this.catalogueDAO.getPoissonDAO().removeOne(especeId);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}
	}

	@Override
	public PoissonVO getVOfromDO(final PoissonDO pdo) {

		final PoissonVO pVo = new PoissonVO();

		pVo.setCode(pdo.getId());
		pVo.setEspece(pdo.getNom());
		pVo.setCouleur(pdo.getCouleur());
		pVo.setDescription(pdo.getDescription());
		pVo.setDimension(pdo.getLongueur() + " x " + pdo.getLargeur());
		pVo.setPrix(pdo.getPrix() + "");

		return pVo;

	}

	@Override
	public PoissonDO getDOfromVO(final PoissonVO pvo) {
		final PoissonDO pDo = new PoissonDO();
		
		if (pvo.getCode() != null) {
			pDo.setId(pvo.getCode());
		}
		pDo.setCouleur(pvo.getCouleur());
		pDo.setDescription(pvo.getDescription());
		pDo.setNom(pvo.getEspece());
		pDo.setPrix(Integer.parseInt(pvo.getPrix()));

		final java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(PoissonVO.REGEX_DIMENSION);
		final java.util.regex.Matcher matcher = pattern.matcher(pvo.getDimension());

		if (matcher.find()) {
			pDo.setLongueur(Float.parseFloat(matcher.group(1)));
			pDo.setLargeur(Float.parseFloat(matcher.group(2)));
		}

		return pDo;
	}

}
