package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.web.RegisterVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Classic action to register a new user
 * 
 * @author Valentin
 *
 */
public class RegisterAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final RegisterVO user = (RegisterVO) form;
		final ActionMessages messages = new ActionMessages(); // To store success/errors/warnings

		try {
			this.catalogueService.getUserService().addOne(user);
			messages.add(MessageType.SUCCESS.toString(),
					new ActionMessage(MessageKey.SUCCESS_ADD_USER, user.getUsername()));
			saveMessages(req, messages);

		} catch (final ServiceException se) {

			se.printError();
			messages.add(MessageType.WARNING.toString(), se.getError(new String[] { user.getUsername() }));
			saveMessages(req, messages);
			return mapping.getInputForward();
		}

		return mapping.findForward(Forward.SUCCESS);

	}

}
