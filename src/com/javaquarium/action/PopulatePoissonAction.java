package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.util.CatalogueService;

/**
 * Action to store the poisson to edit in a session variable in order to
 * populate the edit form
 * 
 * @author Valentin
 *
 */
public class PopulatePoissonAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final HttpSession session = request.getSession();
		final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			// if the parameter is defined
			if (request.getParameter(Param.ESPECE_ID) != null) {
				final int especeId = Integer.parseInt(request.getParameter(Param.ESPECE_ID));
				final PoissonVO poissonVO = this.catalogueService.getPoissonService().getOneById(especeId);
				// if the poisson exists
				if (poissonVO != null)
					session.setAttribute(SessionVar.VAR_TO_EDIT.toString(), poissonVO);
				else {
					messages.add(MessageType.WARNING.toString(),
							new ActionMessage(MessageKey.ERROR_NOEXIST_POISSON, new String[] { especeId + "" }));
					return mapping.findForward(Forward.FAIL);
				}

			}
		} else {
			// message if the user is not logged
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			return mapping.findForward(Forward.NOT_LOGGED);
		}

		saveMessages(request, messages);
		return mapping.findForward(Forward.SUCCESS);
	}

}
