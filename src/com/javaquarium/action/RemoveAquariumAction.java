package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.SessionVar;

/**
 * Action to decrease the quantity of an AquariumPoisson in the Aquarium session
 * variable
 * 
 * @author Valentin
 *
 */
public class RemoveAquariumAction extends Action {

	@Override
	@SuppressWarnings("unchecked")
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		//
		final int especeId = Integer.parseInt(req.getParameter("especeId"));
		final int totalNotSaved = Integer
				.parseInt(req.getSession().getAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()).toString());

		final Map<Integer, AquariumPoissonVO> myAquarium = (Map<Integer, AquariumPoissonVO>) req.getSession()
				.getAttribute(SessionVar.MY_AQUARIUM.toString());

		if (myAquarium.containsKey(especeId)) {
			final AquariumPoissonVO chosen = myAquarium.get(especeId);

			if (chosen.getQuantitySaved() >= 0) {
				if (chosen.getQuantityNotSaved() > (-1) * chosen.getQuantitySaved()) {
					chosen.setQuantityNotSaved(chosen.getQuantityNotSaved() - 1);
					req.getSession().setAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString(), totalNotSaved - 1);
				}

			}
		}

		return mapping.findForward(Forward.SUCCESS);
	}

}
