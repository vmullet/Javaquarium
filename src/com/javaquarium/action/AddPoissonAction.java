package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Classic action to add a new Poisson
 * 
 * @author Valentin
 *
 */
public class AddPoissonAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {

			final PoissonVO poisson = (PoissonVO) form;

			try {
				this.catalogueService.getPoissonService().addOne(poisson);
				req.getSession().removeAttribute(SessionVar.MY_AQUARIUM.toString());
				messages.add(MessageType.SUCCESS.toString(),
						new ActionMessage(MessageKey.SUCCESS_ADD_POISSON, poisson.getEspece()));
				saveMessages(req, messages);
			} catch (final ServiceException se) {
				se.printError();
				messages.add(MessageType.WARNING.toString(), se.getError(new String[] { poisson.getEspece() }));
				saveMessages(req, messages);
				return mapping.getInputForward();
			}
			return mapping.findForward(Forward.SUCCESS);

		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
