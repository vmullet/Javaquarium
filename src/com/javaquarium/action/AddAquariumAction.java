package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;

/**
 * Action to increase the quantity of an AnaquriumPoisson into the Aquarium
 * session variable
 * 
 * @author Valentin
 *
 */
public class AddAquariumAction extends Action {

	@Override
	@SuppressWarnings("unchecked")
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();

		if (session.getAttribute(SessionVar.USER.toString()) != null) {

			try {
				final Integer especeId = Integer.parseInt(req.getParameter(Param.ESPECE_ID));
				final Integer totalNotSaved = Integer
						.parseInt(session.getAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()).toString());
				final Map<Integer, AquariumPoissonVO> myAquarium = (Map<Integer, AquariumPoissonVO>) session
						.getAttribute(SessionVar.MY_AQUARIUM.toString());

				if (myAquarium.containsKey(especeId)) {
					final AquariumPoissonVO chosen = myAquarium.get(especeId);
					chosen.setQuantityNotSaved(chosen.getQuantityNotSaved() + 1);
					session.setAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString(), totalNotSaved + 1);

				} else {
					messages.add(MessageType.ERROR.toString(), new ActionMessage(MessageKey.ERROR_INVALID_ID));
					saveMessages(req, messages);
					return mapping.findForward(Forward.FAIL);
				}
			} catch (final NumberFormatException nfe) {
				messages.add(MessageType.ERROR.toString(), new ActionMessage(MessageKey.ERROR_INVALID_ID));
				saveMessages(req, messages);
				return mapping.findForward(Forward.FAIL);
			}
			return mapping.findForward(Forward.SUCCESS);

		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
