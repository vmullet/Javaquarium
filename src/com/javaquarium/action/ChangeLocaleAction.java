package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.LocaleUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;

/**
 * Action to change the locale of the website
 * 
 * @author Valentin
 *
 */
public class ChangeLocaleAction extends Action {

	/**
	 * Constant name of struts locale variable
	 */
	private static final String STRUTS_LOCALE_KEY = "org.apache.struts.action.LOCALE";

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final ActionMessages messages = new ActionMessages();
		final String locale = request.getParameter(Param.LOCALE_NAME).toString();
		request.getSession().setAttribute(STRUTS_LOCALE_KEY, LocaleUtils.toLocale(locale));
		messages.add(MessageType.INFO.toString(), new ActionMessage(""));

		saveMessages(request, messages);

		// if the user is logged
		if (request.getSession().getAttribute(SessionVar.USER.toString()) != null) {
			return mapping.findForward(Forward.SUCCESS);
		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(request, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
