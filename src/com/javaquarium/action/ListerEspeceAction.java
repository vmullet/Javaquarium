package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Classic Action
 * 
 * @author Valentin
 *
 */
public class ListerEspeceAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final UserVO user = (UserVO) session.getAttribute(SessionVar.USER.toString());
		final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (user != null) {
			try {
				session.setAttribute(SessionVar.TOTAL_POISSONS_SAVED.toString(),
						this.catalogueService.getAquariumPoissonService().getTotalAquariumPoisson(user.getId()));
				// if the aquarium is not already created
				if (session.getAttribute(SessionVar.MY_AQUARIUM.toString()) == null) {
					final Map<Integer, AquariumPoissonVO> myAquarium = this.catalogueService.getAquariumPoissonService()
							.getAllAquariumPoisson(user.getId());
					session.setAttribute(SessionVar.MY_AQUARIUM.toString(), myAquarium);
					session.setAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString(), 0);
				}
				return mapping.findForward(Forward.SUCCESS);
			} catch (final ServiceException se) {
				se.printError();
				messages.add(MessageType.ERROR.toString(), se.getError(null));
				saveMessages(req, messages);
				return mapping.findForward(Forward.FAIL);
			}

		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
