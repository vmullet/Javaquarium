package com.javaquarium.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;

public class LiveAquariumAction extends Action {

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();

		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			final Map<Integer, AquariumPoissonVO> baseAquarium = (Map<Integer, AquariumPoissonVO>) session
					.getAttribute(SessionVar.MY_AQUARIUM.toString());
			if (baseAquarium != null) {
				final List<PoissonVO> liveAquarium = new ArrayList<PoissonVO>();
				for (final AquariumPoissonVO value : baseAquarium.values()) {
					for (int i = 0; i < value.getQuantitySaved(); i++) {
						liveAquarium.add(value.getEspece());
					}
				}
				session.setAttribute(SessionVar.MY_LIVE_AQUARIUM.toString(), liveAquarium);
				return mapping.findForward(Forward.SUCCESS);
			} else {
				return mapping.findForward(Forward.FAIL);
			}
		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
